# ¿Cómo usarlo?
- **Clonar el repositorio**.

        git clone https://PDEZacatecas@bitbucket.org/PDEZacatecas/s1-generador-datos-prueba.git

- **Ir a la carpeta donde se clonó el proyecto e instalar dependencias**.

        npm install

# Generar datos S1
- **Dentro de la carpeta del proyecto ejecutar el siguiente comando**.

        node generarDeclaracion.js -d=100 -t=i

        El argumento "-d" hace referencia al número de declaraciones que se desea generar.
        El argumento "-t" hace referencia al tipo de declaraciones que se desea generar (i=INICIAL), (m=MODIFICACIÓN), (c=CONCLUSIÓN), (t=TODAS).

