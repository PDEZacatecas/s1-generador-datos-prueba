const faker = require('faker/locale/es');
const fs = require('fs');
require('colors');
const argv=require('yargs')
    .option('d',{
        alias:'declaraciones',
        type:'number',
        demandOption:true,
        describe:'Número de declaraciones a generar'
    })
    .option('t',{
        alias:'tipo',
        type:'string',
        demandOption:true,
        describe:'Tipo de declaraciones a generar'
    }).check((argv, options)=>{
        if(isNaN(argv.d)){
            throw 'Declaraciones (d) debe ser un número'
        }

        if(argv.t!='i' && argv.t!='m' && argv.t!='c' && argv.t!='t'){
            throw 'Tipo (t) debe ser i (Inicial), m (Modificación), c (Conclusión), t(todas)'
        }

        return true;
    })
    .argv;

//CATALOGOS------------------------------------------------------------------
const instituciones=["Coordinación General Jurídica","Instituto de la Defensoría Pública","Instituto de Selección y Capacitación del Estado", "Fiscalía General de Justicia del Estado", "Junta Intermunicipal de Agua Potable y Alcantarillado de Zacatecas", "Comisión de Derechos Humanos del Estado"];
const tiposDeclaracion=["INICIAL", "MODIFICACIÓN", "CONCLUSIÓN"];
const extranjero=["EX", "MX"];
const estadosCiviles=[
    {
        "clave": "SOL",
        "valor": "SOLTERO (A)"
    },
    {
        "clave": "CAS",
        "valor": "CASADO (A)"
    },
    {
        "clave": "DIV",
        "valor": "DIVORCIADO (A)"
    },
    {
        "clave": "VIU",
        "valor": "VIUDO (A)"
    },
    {
        "clave": "CON",
        "valor": "CONCUBINA/CONCUBINARIO/UNIÓN LIBRE"
    },
    {
        "clave": "SCO",
        "valor": "SOCIEDAD DE CONVIVENCIA"
    }
];
const regimenesMatrimoniales=[
    {
        "clave": "SOC",
        "valor": "SOCIEDAD CONYUGAL"
    },
    {
        "clave": "SBI",
        "valor": "SEPARACIÓN DE BIENES"
    },
    {
        "clave": "OTR",
        "valor": "OTRO"
    }
];
const tiposOperacion=["AGREGAR", "MODIFICAR", "SIN_CAMBIOS", "BAJA"];
const nivelesEscolaridad=[
    {
        "clave": "PRI",
        "valor": "PRIMARIA"
    },
    {
        "clave": "SEC",
        "valor": "SECUNDARIA"
    },
    {
        "clave": "BCH",
        "valor": "BACHILLERATO"
    },
    {
        "clave": "CTC",
        "valor": "CARRERA TÉCNICA O COMERCIAL"
    },
    {
        "clave": "LIC",
        "valor": "LICENCIATURA"
    },
    {
        "clave": "ESP",
        "valor": "ESPECIALIDAD"
    },
    {
        "clave": "MAE",
        "valor": "MAESTRÍA"
    },
    {
        "clave": "DOC",
        "valor": "DOCTORADO"
    }
];
const domiciliosMexico=[
  {
    "calle": "LAS AMERICAS",
    "numeroExterior": "1622",
    "numeroInterior": "3P",
    "coloniaLocalidad": "VALLE DORADO",
    "municipioAlcaldia": {
        "clave": "001",
        "valor": "AGUASCALIENTES"
    },
    "entidadFederativa": {
        "clave": "01",
        "valor": "AGUASCALIENTES"
    },
    "codigoPostal": "20235"
  },
  {
    "calle": "AVENIDA DE LOS ARCOS",
    "numeroExterior": "767",
    "numeroInterior": "",
    "coloniaLocalidad": "JARDINES DEL BOSQUE",
    "municipioAlcaldia": {
        "clave": "039",
        "valor": "Guadalajara"
    },
    "codigoPostal": "44520",
    "entidadFederativa": {
        "clave": "14",
        "valor": "Jalisco"
    }
  },
  {
    "calle": "Avenida Vallarta",
    "numeroExterior": "1252",
    "numeroInterior": "",
    "coloniaLocalidad": "Americana",
    "municipioAlcaldia": {
        "clave": "039",
        "valor": "Guadalajara"
    },
    "codigoPostal": "44160",
    "entidadFederativa": {
        "clave": "14",
        "valor": "Jalisco"
    }
  },
  {
    "calle": "Hidalgo",
    "numeroExterior": "70",
    "numeroInterior": "",
    "coloniaLocalidad": "Tacoaleche",
    "municipioAlcaldia": {
        "clave": "017",
        "valor": "Guadalupe"
    },
    "codigoPostal": "98630",
    "entidadFederativa": {
        "clave": "32",
        "valor": "Zacatecas"
    }
  }
];
const institucionesEducativas=["IPN", "UNAM", "UTEZ", "UAZ", "UGTO", "BUAP", "UV", "UANL", "UACM", "UABC"];
const ubicaciones=["MX", "EX"];
const carreras=["Sistemas", "Ambiental", "Mecatronica", "Alimentos", "Administración", "Contaduría", "Turismo", "Derecho", "Arquitectura"];
const estatusCarrera=["CURSANDO", "FINALIZADO", "TRUNCO"];
const documentosObtenidos=["BOLETA", "CERTIFICADO", "CONSTANCIA", "TITULO"];
const ordenesGobierno=["FEDERAL","ESTATAL","MUNICIPAL_ALCALDIA"];
const ambitosPublicos=["EJECUTIVO","LEGISLATIVO","JUDICIAL", "ORGANO_AUTONOMO"];
const funcionesPrincipales=["DIRIGIR", "PROGRAMAR", "COORDINAR"];
const relacionesDeclarante=["CONYUGE", "CONCUBINA_CONCUBINARIO_UNION_LIBRE", "SOCIEDAD_DE_CONVIVENCIA"];
const lugaresResidencia=["MEXICO", "EXTRANJERO", "SE_DESCONOCE"];
const actividadesLaborales=[
    {
        "clave": "PUB",
        "valor": "PÚBLICO"
    },
    {
        "clave": "PRI",
        "valor": "PRIVADO"
    },
    {
        "clave": "OTRO",
        "valor": "OTRO"
    },
    {
        "clave": "NIN",
        "valor": "NINGUNO"
    }
];
const monedas=["MXN", "USD", "EUR"];
const empresas=["Patito", "Abarrotes Gomez", "Papeleria Lapicito", "LASEC TELECOMUNICACIONES", "Facebook", "Amazon", "Megacable", "TELMEX", "IZZI"];
const sectores=[
    {
        "clave": "AGRI",
        "valor": "AGRICULTURA"
    },
    {
        "clave": "MIN",
        "valor": "MINERÍA"
    },
    {
        "clave": "EELECT",
        "valor": "ENERGÍA ELÉCTRICA"
    },
    {
        "clave": "CONS",
        "valor": "CONSTRUCCIÓN"
    },
    {
        "clave": "INDMANU",
        "valor": "INDUSTRIA MANUFACTURERA"
    },
    {
        "clave": "CMAYOR",
        "valor": "COMERCIO AL POR MAYOR"
    },
    {
        "clave": "CMENOR",
        "valor": "COMERCIO AL POR MENOR"
    },
    {
        "clave": "TRANS",
        "valor": "TRANSPORTE"
    },
    {
        "clave": "MEDIOM",
        "valor": "MEDIOS MASIVOS"
    },
    {
        "clave": "SERVFIN",
        "valor": "SERVICIOS FINANCIEROS"
    },
    {
        "clave": "SERVINM",
        "valor": "SERVICIOS INMOBILIARIOS"
    },
    {
        "clave": "SERVPROF",
        "valor": "SERVICIOS PROFESIONALES"
    },
    {
        "clave": "SERVCORP",
        "valor": "SERVICIOS CORPORATIVOS"
    },
    {
        "clave": "SERVS",
        "valor": "SERVICIOS DE SALUD"
    },
    {
        "clave": "SERVESPAR",
        "valor": "SERVICIOS DE ESPARCIMIENTO"
    },
    {
        "clave": "SERVALOJ",
        "valor": "SERVICIOS DE ALOJAMIENTO"
    },
    {
        "clave": "OTRO",
        "valor": "OTRO"
    },
];
const parentescos=[
    {
        "clave": "ABU",
        "valor": "ABUELO(A)"
    },
    {
        "clave": "BISA",
        "valor": "BISABUELO(A)"
    },
    {
        "clave": "BISN",
        "valor": "BISNIETO(A)"
    },
    {
        "clave": "CONB",
        "valor": "CONCUBINA O CONCUBINARIO"
    },
    {
        "clave": "CONC",
        "valor": "CONCUÑO(A)"
    },
    {
        "clave": "CONY",
        "valor": "CÓNYUGE"
    },
    {
        "clave": "CUN",
        "valor": "CUÑADO(A)"
    },
    {
        "clave": "HER",
        "valor": "HERMANO(A)"
    },
    {
        "clave": "HIJ",
        "valor": "HIJO(A)"
    },
    {
        "clave": "MAD",
        "valor": "MADRE"
    },
    {
        "clave": "PAD",
        "valor": "PADRE"
    },
    {
        "clave": "PRI",
        "valor": "PRIMO(A)"
    },
    {
        "clave": "SOB",
        "valor": "SOBRINO(A)"
    },
    {
        "clave": "SUE",
        "valor": "SUEGRO(A)"
    },
    {
        "clave": "TATA",
        "valor": "TATARABUELO(A)"
    },
    {
        "clave": "TATN",
        "valor": "TATARANIETO(A)"
    },
    {
        "clave": "TIOA",
        "valor": "TIO(A)"
    },
    {
        "clave": "NIE",
        "valor": "NIETO(A)"
    },
    {
        "clave": "NIN",
        "valor": "NINGUNO"
    },  {
        "clave": "AHI",
        "valor": "AHIJADO(A)"
    },
    {
        "clave": "NUE",
        "valor": "NUERA"
    },
    {
        "clave": "YER",
        "valor": "YERNO"
    },
    {
        "clave": "OTRO",
        "valor": "OTRO"
    }
];
const tiposInstrumento=[
    {
        "clave": "CAP",
        "valor": "CAPITAL"
    },
    {
        "clave": "FIN",
        "valor": "FONDOS DE INVERSIÓN"
    },
    {
        "clave": "OPR",
        "valor": "ORGANIZACIONES PRIVADA"
    },
    {
        "clave": "SSI",
        "valor": "SEGURO DE SEPARACIÓN INDIVIDUALIZADO"
    },
    {
        "clave": "VBU",
        "valor": "VALORES BURSÁTILES"
    },
    {
        "clave": "BON",
        "valor": "BONOS"
    },
    {
        "clave": "OTRO",
        "valor": "OTRO"
    },
];
const tiposServicios=["Limpieza", "Mantenimento tecnico","Instructor"];
const tiposIngreso=["Arrendamiento","Sorteo", "Prestamo","Internet", "Regalía", "Concurso", "Donacion"];
const tiposBien=["MUEBLE", "INMUEBLE","VEHICULO"];
const tiposInmueble=[
    {
        "clave": "CASA",
        "valor": "CASA"
    },
    {
        "clave": "DPTO",
        "valor": "DEPARTAMENTO"
    },
    {
        "clave": "EDIF",
        "valor": "EDIFICIO"
    },
    {
        "clave": "LOCC",
        "valor": "LOCAL COMERCIAL"
    },
    {
        "clave": "BODG",
        "valor": "BODEGA"
    },
    {
        "clave": "PALC",
        "valor": "PALCO"
    },
    {
        "clave": "RACH",
        "valor": "RANCHO"
    },
    {
        "clave": "TERR",
        "valor": "TERRENO"
    },
    {
        "clave": "OTRO",
        "valor": "OTROS"
    },

];
const titulares=[
    {
        "clave": "DEC",
        "valor": "DECLARANTE"
    },
    {
        "clave": "CYG",
        "valor": "CÓNYUGE"
    },
    {
        "clave": "CBN",
        "valor": "CONCUBINA O CONCUBINARIO"
    },
    {
        "clave": "CVV",
        "valor": "CONVIVIENTE"
    },
    {
        "clave": "DEN",
        "valor": "DEPENDIENTE ECONÓMICO"
    },
    {
        "clave": "CTER",
        "valor": "COPROPIEDAD CON TERCEROS"
    }

];
const tiposPersona=["FISICA", "MORAL"];
const formasAdquisicion=[
    {
        "clave": "CPV",
        "valor": "COMPRAVENTA"
    },
    {
        "clave": "CSN",
        "valor": "CESIÓN"
    },
    {
        "clave": "DNC",
        "valor": "DONACIÓN"
    },
    {
        "clave": "HRN",
        "valor": "HERENCIA"
    },
    {
        "clave": "PRM",
        "valor": "PERMUTA"
    },
    {
        "clave": "RST",
        "valor": "RIFA O SORTEO"
    },
    {
        "clave": "STC",
        "valor": "SENTENCIA"
    },

];
const formasPago=["CRÉDITO", "CONTADO", "NO APLICA"];
const valoresConforme=["ESCRITURA PÚBLICA", "SENTENCIA", "CONTRATO"];
const motivosBaja=[
    {
        "clave": "VNT",
        "valor": "VENTA"
    },
    {
        "clave": "DNC",
        "valor": "DONACIÓN"
    },
    {
        "clave": "SNT",
        "valor": "SINIESTRO"
    },
    {
        "clave": "OTRO",
        "valor": "OTRO"
    }
];
const tiposVehiculo=[
    {
        "clave": "AUMOT",
        "valor": "AUTOMÓVIL/MOTOCICLETA"
    },
    {
        "clave": "AERN",
        "valor": "AERONAVE"
    },
    {
        "clave": "BARYA",
        "valor": "BARCO/YATE"
    },
    {
        "clave": "OTRO",
        "valor": "OTROS"
    }
];
const marcas=["Volkswagen", "Nissan", "Ford", "Toyota", "KIA", "Honda", "Mazda"];
const modelos=["Jetta", "March", "Lobo", "Rio", "Golf", "Corsa", "Gol"];
const entidadesFederativas=[
    {
        "clave": "01",
        "valor": "Aguascalientes"
    },
    {
        "clave": "02",
        "valor": "Baja California"
    },
    {
        "clave": "08",
        "valor": "Chihuahua"
    },
    {
        "clave": "13",
        "valor": "Hidalgo"
    },
    {
        "clave": "19",
        "valor": "Nuevo León"
    },
    {
        "clave": "21",
        "valor": "Puebla"
    },
    {
        "clave": "32",
        "valor": "Zacatecas"
    }
];
const tiposBienMueble=[
    {
        "clave": "MECA",
        "valor": "MENAJE DE CASA (MUEBLES Y ACCESORIOS DE CASA)"
    },
    {
        "clave": "APAE",
        "valor": "APARATOS ELECTRÓNICOS Y ELECTRODOMÉSTICOS"
    },
    {
        "clave": "JOYA",
        "valor": "JOYAS"
    },
    {
        "clave": "COLEC",
        "valor": "COLECCIONES"
    },
    {
        "clave": "OBRA",
        "valor": "OBRAS DE ARTE"
    },
    {
        "clave": "OTRO",
        "valor": "OTROS"
    }
];
const tiposInversion=[
    {
        "clave": "BANC",
        "valor": "BANCARIA"
    },
    {
        "clave": "FINV",
        "valor": "FONDOS DE INVERSIÓN"
    },
    {
        "clave": "ORPM",
        "valor": "ORGANIZACIONES PRIVADAS Y/O MERCANTILES"
    },
    {
        "clave": "POMM",
        "valor": "POSESIÓN DE MONEDAS Y/O METALES"
    },
    {
        "clave": "SEGR",
        "valor": "SEGUROS"
    },
    {
        "clave": "VBUR",
        "valor": "VALORES BURSÁTILES"
    },
    {
        "clave": "AFOT",
        "valor": "AFORES Y OTROS"
    }
];
const subTiposInversion=[
    {
        "clave": "CNOM",
        "valor": "CUENTA DE NÓMINA"
    },
    {
        "clave": "CAHO",
        "valor": "CUENTA DE AHORRO"
    },
    {
        "clave": "SOIN",
        "valor": "SOCIEDADES DE INVERSIÓN"
    },
    {
        "clave": "DIVS",
        "valor": "DIVISAS"
    },
    {
        "clave": "MONN",
        "valor": "MONEDA NACIONAL"
    },
    {
        "clave": "SEGV",
        "valor": "SEGURO DE VIDA"
    },
    {
        "clave": "AFOR",
        "valor": "AFORES"
    },
    {
        "clave": "FIDE",
        "valor": "FIDEICOMISOS"
    }
];
const tiposAdeudo=[
    {
        "clave": "CHIP",
        "valor": "CRÉDITO HIPOTECARIO"
    },
    {
        "clave": "CAUT",
        "valor": "CRÉDITO AUTOMOTRIZ"
    },
    {
        "clave": "CPER",
        "valor": "CRÉDITO PERSONAL"
    },
    {
        "clave": "TCRN",
        "valor": "TARJETA DE CRÉDITO BANCARIA"
    },
    {
        "clave": "TCRD",
        "valor": "TARJETA DE CRÉDITO DEPARTAMENTAL"
    },
    {
        "clave": "PRPE",
        "valor": "PRÉSTAMO PERSONAL"
    },
    {
        "clave": "OTRO",
        "valor": "OTROS"
    }
];
const tiposRelacion=["DECLARANTE", "PAREJA", "DEPENDIENTE_ECONOMICO"];
const tiposParticipacion=[
    {
        "clave": "SCIO",
        "valor": "SOCIO"
    },
    {
        "clave": "ACCI",
        "valor": "ACCIONISTA"
    },
    {
        "clave": "COMI",
        "valor": "COMISARIO"
    },
    {
        "clave": "REPR",
        "valor": "REPRESENTANTE"
    },
    {
        "clave": "APOD",
        "valor": "APODERADO"
    },
    {
        "clave": "COLB",
        "valor": "COLABORADOR"
    },
    {
        "clave": "BENE",
        "valor": "BENEFICIARIO"
    },
    {
        "clave": "OTRO",
        "valor": "OTROS"
    }
];
const tiposInstitucion=[
    {
        "clave": "OSC",
        "valor": "ORGANIZACIONES DE LA SOCIEDAD CIVIL"
    },
    {
        "clave": "OB",
        "valor": "ORGANIZACIONES BENÉFICAS"
    },
    {
        "clave": "PP",
        "valor": "PARTIDOS POLÍTICOS"
    },
    {
        "clave": "GS",
        "valor": "GREMIOS/SINDICATOS"
    },
    {
        "clave": "OTRO",
        "valor": "OTRO"
    }
];
const beneficiarios=[
    {
        "clave": "DC",
        "valor": "DECLARANTE"
    },
    {
        "clave": "CY",
        "valor": "CÓNYUGE"
    },
    {
        "clave": "CON",
        "valor": "CONCUBINA O CONCUBINARIO"
    },
    {
        "clave": "CONV",
        "valor": "CONVIVIENTE"
    },
    {
        "clave": "HIJ",
        "valor": "HIJO(A)"
    },
    {
        "clave": "HER",
        "valor": "HERMANO(A)"
    },
    {
        "clave": "CU",
        "valor": "CUÑADO(A)"
    },
    {
        "clave": "MA",
        "valor": "MADRE"
    },
    {
        "clave": "PA",
        "valor": "PADRE"
    },
    {
        "clave": "TIO",
        "valor": "TÍO(A)"
    },
    {
        "clave": "PRI",
        "valor": "PRIMO(A)"
    },
    {
        "clave": "SOB",
        "valor": "SOBRINO(A)"
    },
    {
        "clave": "AHI",
        "valor": "AHIJADO(A)"
    },
    {
        "clave": "NUE",
        "valor": "NUERA"
    },
    {
        "clave": "YER",
        "valor": "YERNO"
    },
    {
        "clave": "ABU",
        "valor": "ABUELO(A)"
    },
    {
        "clave": "NIE",
        "valor": "NIETO(A)"
    },
    {
        "clave": "OTRO",
        "valor": "OTRO(A)"
    }
];
const tiposApoyo=[
    {
        "clave": "SUB",
        "valor": "SUBSIDIO"
    },
    {
        "clave": "SER",
        "valor": "SERVICIO"
    },
    {
        "clave": "OBRA",
        "valor": "OBRA"
    },
    {
        "clave": "OTRO",
        "valor": "OTRO"
    }
];
const formasRecepcion=["MONETARIO", "ESPECIE"];
const tiposRepresentacion=["REPRESENTANTE", "REPRESENTADO"];
const tiposBeneficio=[
    {
        "clave": "S",
        "valor": "SORTEO"
    },
    {
        "clave": "C",
        "valor": "CONCURSO"
    },
    {
        "clave": "D",
        "valor": "DONACIÓN"
    },
    {
        "clave": "O",
        "valor": "OTRO"
    }
];
const tiposFideicomisos=["PUBLICO", "PRIVADO", "MIXTO"];
const tiposParticipacionFideicomisos=["FIDEICOMITENTE", "FIDUCIARIO", "FIDEICOMISARIO", "COMITE_TECNICO"];

//GENERADORES GENERALES-----------------------------------------------------------------------------------------------
const darFormatoFecha=(fecha)=> {
    let nueva = new Date(fecha);
    nueva.setDate(nueva.getDate() + 1);
    let month = '' + (nueva.getMonth() + 1),
        day = '' + (nueva.getDate()),
        year = nueva.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

const  generarString = (num) => {
    let cadena=`${faker.name.firstName()}${faker.name.firstName()}${faker.name.firstName()}`.toUpperCase()
    return cadena.slice(0,num).normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

const generarRFC=(tipo)=>{
    //0 objeto
    //1 completo
    if(tipo==0){
        return {
            rfc:`${generarString(4).toUpperCase()}${faker.datatype.number({ min: 100000, max:  999999})}`.trim(),
            homoClave:faker.random.alphaNumeric(3).toUpperCase()
        }
    }else{
        return `${generarString(4).toUpperCase()}${faker.datatype.number({ min: 100000, max:  999999})}${faker.random.alphaNumeric(3).toUpperCase()}`.trim()
    }
    
}

const generarCurp=()=>{
    return `${generarString(4).toUpperCase()}${faker.datatype.number({ min: 100000, max:  999999})}${faker.random.arrayElement(["M", "F"])}${generarString(5).toUpperCase()}${faker.datatype.number({ min: 10, max:  99})}`
}

const generarDomicilioExtranjero=()=>{
    return {
        calle: faker.address.streetName(),
        numeroExterior:faker.datatype.number({ min: 1, max:  500}).toString(),
        numeroInterior:faker.datatype.number({ min: 1, max:  500}).toString(),
        ciudadLocalidad: faker.address.city(),
        estadoProvincia: faker.address.state(),
        pais:faker.address.countryCode(),
        codigoPostal: faker.address.zipCode()
    }
}

const generarEscolaridad=() =>{
    let arregloEscolaridad=[];
    let limite=faker.datatype.number({ min: 1, max:  2});
    for (let i = 0; i < limite; i++) {
        const escolaridad = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            nivel:faker.random.arrayElement(nivelesEscolaridad),
            institucionEducativa:{
                nombre:faker.random.arrayElement(institucionesEducativas),
                ubicacion:faker.random.arrayElement(ubicaciones),
            },
            carreraAreaConocimiento:faker.random.arrayElement(carreras),
            estatus:faker.random.arrayElement(estatusCarrera),
            documentoObtenido:faker.random.arrayElement(documentosObtenidos),
            fechaObtencion:darFormatoFecha(faker.date.past()),
        }

        arregloEscolaridad.push(escolaridad)
    }

    return arregloEscolaridad;
}

const generarMonto=() =>{
    return {
        valor:faker.datatype.number({ min: 1000, max:  1000000}),
        moneda:faker.random.arrayElement(monedas),
    }
}

const generarTitulares=() =>{

    let arregloTitulares=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i <  limite; i++) {
        arregloTitulares.push(faker.random.arrayElement(titulares))
        arregloTitulares=Array.from(new Set(arregloTitulares));
            
    }
    return arregloTitulares;
}

const generarTerceros=() =>{

    let arregloTerceros=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i <  limite; i++) {
       const tercero={
           tipoPersona:faker.random.arrayElement(tiposPersona),
           nombreRazonSocial:faker.random.arrayElement(empresas),
           rfc:generarRFC(1)
       }

       arregloTerceros.push(tercero);
            
    }
    return arregloTerceros;
}

const generarTransmisor=() =>{

    let arregloTransmisor=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i <  limite; i++) {
       const transmisor={
           tipoPersona:faker.random.arrayElement(tiposPersona),
           nombreRazonSocial:faker.random.arrayElement(empresas),
           rfc:generarRFC(1),
           relacion:faker.random.arrayElement(parentescos),
       }

       arregloTransmisor.push(transmisor);
            
    }
    return arregloTransmisor;
}

const generarMedida=() =>{
    return {
        valor:faker.datatype.number({ min: 50, max:  1000}),
        unidad:"m2",
    }
}

//GENERADORES ATRIBUTOS DECLARACION------------------------------------------
const generarMetadata=(tipo)=>{
    return {
        actualizacion: generarActualizacion(),
        institucion:faker.random.arrayElement(instituciones),
        tipo:tipo
    }
}

const generarDatosGenerales=()=>{
    return {
        nombre:faker.name.firstName(),
        primerApellido:faker.name.lastName(),
        segundoApellido:faker.name.lastName(),
        curp:generarCurp(),
        rfc:generarRFC(0),
        correoElectronico:{
            institucional:faker.internet.email(),
            personal:faker.internet.email()
        },
        telefono:{
            casa:faker.datatype.number({ min: 1000000000, max:  9999999999}).toString(),
            celularPersonal:faker.datatype.number({ min: 1000000000, max:  9999999999}).toString()
        },
        situacionPersonalEstadoCivil:faker.random.arrayElement(estadosCiviles),
        regimenMatrimonial:faker.random.arrayElement(regimenesMatrimoniales),
        paisNacimiento:faker.address.countryCode(),
        nacionalidad:faker.address.countryCode(),
        aclaracionesObservaciones:faker.random.words(8)
    }
}

const generarDomicilioDeclarante=()=>{
    return {
        domicilioMexico:faker.random.arrayElement(domiciliosMexico),
        domicilioExtranjero:generarDomicilioExtranjero(),
        aclaracionesObservaciones:faker.random.words(8)
    }
}

const generarDatosCurriculares=()=>{
    return {
        escolaridad:generarEscolaridad(faker.datatype.number({ min: 1, max:  2})),
        aclaracionesObservaciones:faker.random.words(8)
    }
}

const generarDatosEmpleoCargoComision=(tipo)=>{
    //1 Modificacion
    if(tipo==1){
        let otroEmpleo=faker.datatype.boolean();

        if(otroEmpleo==false){
            return {
                tipoOperacion:faker.random.arrayElement(tiposOperacion),
                nivelOrdenGobierno:faker.random.arrayElement(ordenesGobierno),
                ambitoPublico:faker.random.arrayElement(ambitosPublicos),
                nombreEntePublico:faker.random.arrayElement(instituciones),
                areaAdscripcion:faker.name.jobArea(),
                empleoCargoComision:faker.name.jobType(),
                contratadoPorHonorarios:faker.datatype.boolean(),
                nivelEmpleoCargoComision:faker.random.alphaNumeric(3).toUpperCase(),
                funcionPrincipal:faker.random.arrayElement(funcionesPrincipales),
                fechaTomaPosesion:darFormatoFecha(faker.date.past()),
                telefonoOficina:{
                    telefono:faker.datatype.number({ min: 1000000000, max:  9999999999}).toString(),
                    extension:faker.datatype.number({ min: 1000, max:  9999}).toString()
                },
                domicilioMexico:faker.random.arrayElement(domiciliosMexico),
                domicilioExtranjero:generarDomicilioExtranjero(),
                aclaracionesObservaciones:faker.random.words(8),
                cuentaConOtroCargoPublico:false
            };
        }else{
            let arregloEmpleos=[];
            let limite=faker.datatype.number({ min: 1, max:  2});
    
            for (let i = 0; i < limite; i++) {
                const otroEmpleo = {
                        nivelOrdenGobierno:faker.random.arrayElement(ordenesGobierno),
                        ambitoPublico:faker.random.arrayElement(ambitosPublicos),
                        nombreEntePublico:faker.random.arrayElement(instituciones),
                        areaAdscripcion:faker.name.jobArea(),
                        empleoCargoComision:faker.name.jobType(),
                        contratadoPorHonorarios:faker.datatype.boolean(),
                        nivelEmpleoCargoComision:faker.random.alphaNumeric(3).toUpperCase(),
                        funcionPrincipal:faker.random.arrayElement(funcionesPrincipales),
                        fechaTomaPosesion:darFormatoFecha(faker.date.past()),
                        telefonoOficina:{
                            telefono:faker.datatype.number({ min: 1000000000, max:  9999999999}).toString(),
                            extension:faker.datatype.number({ min: 1000, max:  9999}).toString()
                        },
                        domicilioMexico:faker.random.arrayElement(domiciliosMexico),
                        domicilioExtranjero:generarDomicilioExtranjero(),
                        aclaracionesObservaciones:faker.random.words(8),
                    }
        
                    arregloEmpleos.push(otroEmpleo)
            }
    
            return {
                tipoOperacion:faker.random.arrayElement(tiposOperacion),
                nivelOrdenGobierno:faker.random.arrayElement(ordenesGobierno),
                ambitoPublico:faker.random.arrayElement(ambitosPublicos),
                nombreEntePublico:faker.random.arrayElement(instituciones),
                areaAdscripcion:faker.name.jobArea(),
                empleoCargoComision:faker.name.jobType(),
                contratadoPorHonorarios:faker.datatype.boolean(),
                nivelEmpleoCargoComision:faker.random.alphaNumeric(3).toUpperCase(),
                funcionPrincipal:faker.random.arrayElement(funcionesPrincipales),
                fechaTomaPosesion:darFormatoFecha(faker.date.past()),
                telefonoOficina:{
                    telefono:faker.datatype.number({ min: 1000000000, max:  9999999999}).toString(),
                    extension:faker.datatype.number({ min: 1000, max:  9999}).toString()
                },
                domicilioMexico:faker.random.arrayElement(domiciliosMexico),
                domicilioExtranjero:generarDomicilioExtranjero(),
                aclaracionesObservaciones:faker.random.words(8),
                cuentaConOtroCargoPublico:true,
                otroEmpleoCargoComision:arregloEmpleos
            };
        }
    }else{
        return {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            nivelOrdenGobierno:faker.random.arrayElement(ordenesGobierno),
            ambitoPublico:faker.random.arrayElement(ambitosPublicos),
            nombreEntePublico:faker.random.arrayElement(instituciones),
            areaAdscripcion:faker.name.jobArea(),
            empleoCargoComision:faker.name.jobType(),
            contratadoPorHonorarios:faker.datatype.boolean(),
            nivelEmpleoCargoComision:faker.random.alphaNumeric(3).toUpperCase(),
            funcionPrincipal:faker.random.arrayElement(funcionesPrincipales),
            fechaTomaPosesion:darFormatoFecha(faker.date.past()),
            telefonoOficina:{
                telefono:faker.datatype.number({ min: 1000000000, max:  9999999999}).toString(),
                extension:faker.datatype.number({ min: 1000, max:  9999}).toString()
            },
            domicilioMexico:faker.random.arrayElement(domiciliosMexico),
            domicilioExtranjero:generarDomicilioExtranjero(),
            aclaracionesObservaciones:faker.random.words(8),
        }
    }
}

const generarExperiencia=() =>{
    let arregloExperiencia=[];
    let limite=faker.datatype.number({ min: 1, max:  3});

    for (let i = 0; i < limite; i++) {
        let trabajo={};
        if(i%2==0){
            trabajo = {
                tipoOperacion:faker.random.arrayElement(tiposOperacion),
                ambitoSector:{
                    clave:"PUB",
                    valor:"Público"
                },
                nivelOrdenGobierno:faker.random.arrayElement(ordenesGobierno),
                ambitoPublico:faker.random.arrayElement(ambitosPublicos),
                nombreEntePublico:faker.random.arrayElement(instituciones),
                areaAdscripcion:faker.name.jobArea(),
                empleoCargoComision:faker.name.jobType(),
                funcionPrincipal:faker.random.arrayElement(funcionesPrincipales),
                fechaIngreso:darFormatoFecha(faker.date.past()),
                fechaEgreso:darFormatoFecha(faker.date.past()),
                ubicacion:faker.random.arrayElement(ubicaciones),
            }
        }else{
            trabajo = {
                tipoOperacion:faker.random.arrayElement(tiposOperacion),
                ambitoSector:{
                    clave:"PRV",
                    valor:"Privado"
                },
                nombreEmpresaSociedadAsociacion:faker.random.arrayElement(empresas),
                rfc:generarRFC(1),
                area:faker.name.jobArea(), 
                puesto:faker.name.jobType(),
                sector:faker.random.arrayElement(sectores),
                fechaIngreso:darFormatoFecha(faker.date.past()),
                fechaEgreso:darFormatoFecha(faker.date.past()),
                ubicacion:faker.random.arrayElement(ubicaciones)
            }
        }
        arregloExperiencia.push(trabajo)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            experiencia:arregloExperiencia,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            experiencia:[]
        };
    }
}

const generarActividadLaboralSectorPublico=()=>{
    return {
        nivelOrdenGobierno:faker.random.arrayElement(ordenesGobierno),
        ambitoPublico:faker.random.arrayElement(ambitosPublicos),
        nombreEntePublico:faker.random.arrayElement(instituciones),
        areaAdscripcion:faker.name.jobArea(),
        empleoCargoComision:faker.name.jobType(),
        funcionPrincipal:faker.random.arrayElement(funcionesPrincipales),
        salarioMensualNeto:generarMonto(),
        fechaIngreso:darFormatoFecha(faker.date.past()),
    }
}

const generarDatosPareja=() =>{
    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            nombre:faker.name.firstName(),
            primerApellido:faker.name.lastName(),
            segundoApellido:faker.name.lastName(),
            fechaNacimiento:darFormatoFecha(faker.date.past()),
            rfc:generarRFC(1),
            relacionConDeclarante:faker.random.arrayElement(relacionesDeclarante),
            ciudadanoExtranjero:faker.datatype.boolean(),
            curp:generarCurp(),
            esDependienteEconomico:faker.datatype.boolean(),
            habitaDomicilioDeclarante:faker.datatype.boolean(),
            lugarDondeReside:faker.random.arrayElement(lugaresResidencia),
            domicilioMexico:faker.random.arrayElement(domiciliosMexico),
            domicilioExtranjero:generarDomicilioExtranjero(),
            actividadLaboral:faker.random.arrayElement(actividadesLaborales),
            actividadLaboralSectorPublico:generarActividadLaboralSectorPublico(),
            actividadLaboralSectorPrivadoOtro:{
                nombreEmpresaSociedadAsociacion:faker.random.arrayElement(empresas),
                empleoCargoComision:faker.name.jobType(),
                rfc:generarRFC(1),
                fechaIngreso:darFormatoFecha(faker.date.past()),
                sector:faker.random.arrayElement(sectores),
                salarioMensualNeto:generarMonto(),
                proveedorContratistaGobierno:faker.datatype.boolean(),
            },
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera
        };
    }
}

const generarDatosDependienteEconomico=() =>{

    let arregloDependendientes=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const dependiente = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            nombre:faker.name.firstName(),
            primerApellido:faker.name.lastName(),
            segundoApellido:faker.name.lastName(),
            fechaNacimiento:darFormatoFecha(faker.date.past()),
            rfc:generarRFC(1),
            parentescoRelacion:faker.random.arrayElement(parentescos),
            extranjero:faker.datatype.boolean(),
            curp:generarCurp(),
            habitaDomicilioDeclarante:faker.datatype.boolean(),
            lugarDondeReside:"MEXICO",
            domicilioMexico:faker.random.arrayElement(domiciliosMexico),
            domicilioExtranjero:generarDomicilioExtranjero(),
            actividadLaboral:faker.random.arrayElement(actividadesLaborales),
            actividadLaboralSectorPublico:generarActividadLaboralSectorPublico(),
            actividadLaboralSectorPrivadoOtro:{
                nombreEmpresaSociedadAsociacion:faker.random.arrayElement(empresas),
                rfc:generarRFC(1),
                empleoCargo:faker.name.jobType(),
                fechaIngreso:darFormatoFecha(faker.date.past()),
                salarioMensualNeto:generarMonto(),
            },
            proveedorContratistaGobierno:faker.datatype.boolean(),
            sector:faker.random.arrayElement(sectores),
        }

        arregloDependendientes.push(dependiente)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            dependienteEconomico:arregloDependendientes,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            dependienteEconomico:[]
        };
    }
}

const generarActividadesIndustriales=() =>{

    let arregloActividades=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const actividad = {
            remuneracion:generarMonto(),
            nombreRazonSocial:faker.random.arrayElement(empresas),
            tipoNegocio:"Venta de productos"
        }

        arregloActividades.push(actividad)
    }
    return arregloActividades;
}

const generarActividadesFinancieras=() =>{

    let arregloActividades=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const actividad = {
            remuneracion:generarMonto(),
            tipoInstrumento:faker.random.arrayElement(tiposInstrumento)
        }

        arregloActividades.push(actividad)
    }
    return arregloActividades;
}

const generarServiciosProfesionales=() =>{

    let arregloServicios=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const servicio = {
            remuneracion:generarMonto(),
            tipoServicio:faker.random.arrayElement(tiposServicios)
        }

        arregloServicios.push(servicio)
    }
    return arregloServicios;
}

const generarIngresos=() =>{

    let arregloIngresos=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const ingreso = {
            remuneracion:generarMonto(),
            tipoIngreso:faker.random.arrayElement(tiposIngreso)
        }

        arregloIngresos.push(ingreso)
    }
    return arregloIngresos;
}

const generarBienes=() =>{

    let arregloBienes=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const bien = {
            remuneracion:generarMonto(),
            tipoBienEnajenado:faker.random.arrayElement(tiposBien)
        }

        arregloBienes.push(bien)
    }
    return arregloBienes;
}

const generarActividadAnualInterior=() =>{
    let bandera=faker.datatype.boolean();

    if(bandera==true){
        return {
            servidorPublicoAnioAnterior:true,
            fechaIngreso:darFormatoFecha(faker.date.past()),
            fechaConclusion:darFormatoFecha(faker.date.recent()),
            remuneracionNetaCargoPublico:generarMonto(),
            otrosIngresosTotal:generarMonto(),
            actividadIndustialComercialEmpresarial:{
                remuneracionTotal:generarMonto(),
                actividades:generarActividadesIndustriales(),
            },
            actividadFinanciera:{
                remuneracionTotal:generarMonto(),
                actividades:generarActividadesFinancieras(),
            },
            serviciosProfesionales:{
                remuneracionTotal:generarMonto(),
                servicios:generarServiciosProfesionales(),
            },
            enajenacionBienes:{
                remuneracionTotal:generarMonto(),
                bienes:generarBienes(),
            },
            otrosIngresos:{
                remuneracionTotal:generarMonto(),
                ingresos:generarIngresos()
            },
            ingresoNetoAnualDeclarante:generarMonto(),
            ingresoNetoAnualParejaDependiente:generarMonto(),
            totalIngresosNetosAnuales:generarMonto(), 
            aclaracionesObservaciones:faker.random.words(8), 
        };
    }else{
          return {
            servidorPublicoAnioAnterior:false
          };
    }
}

const generarBienesInmuebles=() =>{
    let arregloBienes=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const bien = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            tipoInmueble:faker.random.arrayElement(tiposInmueble),
            titular:generarTitulares(),
            porcentajePropiedad:faker.datatype.number({ min: 1, max:  100}),
            superficieTerreno:generarMedida(),
            superficieConstruccion:generarMedida(),
            tercero:generarTerceros(),
            transmisor:generarTransmisor(),
            formaAdquisicion:faker.random.arrayElement(formasAdquisicion),
            formaPago:faker.random.arrayElement(formasPago),
            valorAdquisicion:generarMonto(),
            fechaAdquisicion:darFormatoFecha(faker.date.past()),
            datoIdentificacion:faker.random.alphaNumeric(5).toUpperCase(),
            valorConformeA:faker.random.arrayElement(valoresConforme),
            domicilioMexico:faker.random.arrayElement(domiciliosMexico),
            domicilioExtranjero:generarDomicilioExtranjero(),
            motivoBaja:faker.random.arrayElement(motivosBaja),
        }

        arregloBienes.push(bien)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            bienInmueble:arregloBienes,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            bienInmueble:[]
        };
    }
}

const generarVehiculos=() =>{
    let arregloVehiculos=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const vehiculo = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            tipoVehiculo:faker.random.arrayElement(tiposVehiculo),
            titular:generarTitulares(),
            transmisor:generarTransmisor(),
            marca:faker.random.arrayElement(marcas),
            modelo:faker.random.arrayElement(modelos),
            anio:faker.datatype.number({ min: 1900, max:  2021}),
            numeroSerieRegistro:faker.random.alphaNumeric(5).toUpperCase(),
            tercero:generarTerceros(),
            lugarRegistro:{
                pais:"MX",
                entidadFederativa:faker.random.arrayElement(entidadesFederativas),
            },
            formaAdquisicion:faker.random.arrayElement(formasAdquisicion),
            formaPago:faker.random.arrayElement(formasPago),
            valorAdquisicion:generarMonto(),
            fechaAdquisicion:darFormatoFecha(faker.date.past()),
            motivoBaja:faker.random.arrayElement(motivosBaja),
        }

        arregloVehiculos.push(vehiculo)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            vehiculo:arregloVehiculos,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            vehiculo:[]
        };
    }
}

const generarBienesMuebles=() =>{
    let arregloBienesMuebles=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const bien = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            titular:generarTitulares(),
            tipoBien:faker.random.arrayElement(tiposBienMueble),
            transmisor:generarTransmisor(),
            tercero:generarTerceros(),
            descripcionGeneralBien:faker.random.words(8),
            formaAdquisicion:faker.random.arrayElement(formasAdquisicion),
            formaPago:faker.random.arrayElement(formasPago),
            valorAdquisicion:generarMonto(),
            fechaAdquisicion:darFormatoFecha(faker.date.past()),
            motivoBaja:faker.random.arrayElement(motivosBaja)
        }

        arregloBienesMuebles.push(bien)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            bienMueble:arregloBienesMuebles,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            bienMueble:[]
        };
    }
}

const generarInversiones=(tipo) =>{
    
    //0 INICIAL
    //1 MODIFICACION
    //2 CONCLUSION
    let arregloInversiones=[];
    let limite=faker.datatype.number({ min: 1, max:  2});
    let inversion={};
    for (let i = 0; i < limite; i++) {

        switch (tipo) {
            case 0:
                inversion = {
                    tipoOperacion:faker.random.arrayElement(tiposOperacion),
                    tipoInversion:faker.random.arrayElement(tiposInversion),
                    subTipoInversion:faker.random.arrayElement(subTiposInversion),
                    titular:generarTitulares(),
                    tercero:generarTerceros(),
                    numeroCuentaContrato:faker.random.alphaNumeric(5).toUpperCase(),
                    localizacionInversion:{
                        pais:faker.address.countryCode(),
                        institucionRazonSocial:faker.random.arrayElement(empresas),
                        rfc:generarRFC(1)
                    },
                    saldoSituacionActual:generarMonto(),
                }
            break;

            case 1:
                inversion = {
                    tipoOperacion:faker.random.arrayElement(tiposOperacion),
                    tipoInversion:faker.random.arrayElement(tiposInversion),
                    subTipoInversion:faker.random.arrayElement(subTiposInversion),
                    titular:generarTitulares(),
                    tercero:generarTerceros(),
                    numeroCuentaContrato:faker.random.alphaNumeric(5).toUpperCase(),
                    localizacionInversion:{
                        pais:faker.address.countryCode(),
                        institucionRazonSocial:faker.random.arrayElement(empresas),
                        rfc:generarRFC(1)
                    },
                    saldoDiciembreAnterior:generarMonto(),
                    porcentajeIncrementoDecremento:faker.datatype.number({ min: 1, max:  100}),
                }
            break;

            case 2:
                inversion = {
                    tipoOperacion:faker.random.arrayElement(tiposOperacion),
                    tipoInversion:faker.random.arrayElement(tiposInversion),
                    subTipoInversion:faker.random.arrayElement(subTiposInversion),
                    titular:generarTitulares(),
                    tercero:generarTerceros(),
                    numeroCuentaContrato:faker.random.alphaNumeric(5).toUpperCase(),
                    localizacionInversion:{
                        pais:faker.address.countryCode(),
                        institucionRazonSocial:faker.random.arrayElement(empresas),
                        rfc:generarRFC(1)
                    },
                    saldoFechaConclusion:generarMonto(),
                    porcentajeIncrementoDecremento:faker.datatype.number({ min: 1, max:  100}),
                }
            break;
        }
    
        arregloInversiones.push(inversion)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            inversion:arregloInversiones,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            inversion:[]
        };
    }
}

const generarAdeudos=(tipo) =>{
    
    //0 INICIAL
    //1 MODIFICACION
    //2 CONCLUSION
    let arregloAdeudos=[];
    let limite=faker.datatype.number({ min: 1, max:  2});
    let adeudo={};
    for (let i = 0; i < limite; i++) {

        switch (tipo) {
            case 0:
                adeudo = {
                    tipoOperacion:faker.random.arrayElement(tiposOperacion),
                    titular:generarTitulares(),
                    tipoAdeudo:faker.random.arrayElement(tiposAdeudo),
                    numeroCuentaContrato:faker.random.alphaNumeric(5).toUpperCase(),
                    fechaAdquisicion:darFormatoFecha(faker.date.past()),
                    montoOriginal:generarMonto(),
                    saldoInsolutoSituacionActual:generarMonto(),
                    tercero:generarTerceros(),
                    otorganteCredito:{
                        tipoPersona:faker.random.arrayElement(tiposPersona),
                        nombreInstitucion:faker.random.arrayElement(instituciones),
                        rfc:generarRFC()
                    },
                    localizacionAdeudo:{
                        pais:faker.address.countryCode()
                    }
                }
            break;

            case 1:
                adeudo = {
                    tipoOperacion:faker.random.arrayElement(tiposOperacion),
                    titular:generarTitulares(),
                    tipoAdeudo:faker.random.arrayElement(tiposAdeudo),
                    numeroCuentaContrato:faker.random.alphaNumeric(5).toUpperCase(),
                    fechaAdquisicion:darFormatoFecha(faker.date.past()),
                    montoOriginal:generarMonto(),
                    saldoInsolutoDiciembreAnterior:generarMonto(),
                    porcentajeIncrementoDecremento:faker.datatype.number({ min: 1, max:  100}),
                    tercero:generarTerceros(),
                    otorganteCredito:{
                        tipoPersona:faker.random.arrayElement(tiposPersona),
                        nombreInstitucion:faker.random.arrayElement(instituciones),
                        rfc:generarRFC()
                    },
                    localizacionAdeudo:{
                        pais:faker.address.countryCode()
                    }
                }
            break;

            case 2:
                adeudo = {
                    tipoOperacion:faker.random.arrayElement(tiposOperacion),
                    titular:generarTitulares(),
                    tipoAdeudo:faker.random.arrayElement(tiposAdeudo),
                    numeroCuentaContrato:faker.random.alphaNumeric(5).toUpperCase(),
                    fechaAdquisicion:darFormatoFecha(faker.date.past()),
                    montoOriginal:generarMonto(),
                    saldoInsolutoFechaConclusion:generarMonto(),
                    porcentajeIncrementoDecremento:faker.datatype.number({ min: 1, max:  100}),
                    tercero:generarTerceros(),
                    otorganteCredito:{
                        tipoPersona:faker.random.arrayElement(tiposPersona),
                        nombreInstitucion:faker.random.arrayElement(instituciones),
                        rfc:generarRFC()
                    },
                    localizacionAdeudo:{
                        pais:faker.address.countryCode()
                    }
                }
            break;
        }
    
        arregloAdeudos.push(adeudo)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            adeudo:arregloAdeudos,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            adeudo:[]
        };
    }
}

const generarPrestamosOComodatos=() =>{
    let arregloPrestamos=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const prestamo = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            tipoBien:{
                inmueble:{
                    tipoInmueble:faker.random.arrayElement(tiposInmueble),
                    domicilioMexico:faker.random.arrayElement(domiciliosMexico),
                    domicilioExtranjero:generarDomicilioExtranjero(),
                },
                vehiculo:{
                    tipo:faker.random.arrayElement(tiposVehiculo),
                    marca:faker.random.arrayElement(marcas),
                    modelo:faker.random.arrayElement(modelos),
                    anio:faker.datatype.number({ min: 1900, max:  2021}),
                    numeroSerieRegistro:faker.random.alphaNumeric(5).toUpperCase(),
                    lugarRegistro:{
                        pais:"MX",
                        entidadFederativa:faker.random.arrayElement(entidadesFederativas),
                    }
                }
            },
            duenoTitular:{
                tipoDuenoTitular:faker.random.arrayElement(tiposPersona),
                nombreTitular:faker.name.firstName(),
                rfc:generarRFC(1),
                relacionConTitular:faker.random.arrayElement(["Amigo", "Cuñado"]),
            }
        }

        arregloPrestamos.push(prestamo)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            prestamo:arregloPrestamos,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            prestamo:[]
        };
    }
}

const generarParticipaciones=() =>{
    let arregloParticipaciones=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const participacion = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            tipoRelacion:faker.random.arrayElement(tiposRelacion),
            nombreEmpresaSociedadAsociacion:faker.random.arrayElement(empresas),
            rfc:generarRFC(1),
            porcentajeParticipacion:faker.datatype.number({ min: 1, max:  100}),
            tipoParticipacion:faker.random.arrayElement(tiposParticipacion),
            recibeRemuneracion:faker.datatype.boolean(),
            montoMensual:generarMonto(),
            ubicacion:{
                pais:"MX",
                entidadFederativa:faker.random.arrayElement(entidadesFederativas),
            },
            sector:faker.random.arrayElement(sectores),
        }

        arregloParticipaciones.push(participacion)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            participacion:arregloParticipaciones,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            participacion:[]
        };
    }
}

const generarParticipacionesTomaDecisiones=() =>{
    let arregloParticipaciones=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const participacion = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            tipoRelacion:faker.random.arrayElement(tiposRelacion),
            tipoInstitucion:faker.random.arrayElement(tiposInstitucion),
            nombreInstitucion:faker.random.arrayElement(instituciones),
            rfc:generarRFC(1),
            puestoRol:faker.name.jobType(),
            fechaInicioParticipacion:darFormatoFecha(faker.date.past()),
            recibeRemuneracion:faker.datatype.boolean(),
            montoMensual:generarMonto(),
            ubicacion:{
                pais:"MX",
                entidadFederativa:faker.random.arrayElement(entidadesFederativas),
            },
        }

        arregloParticipaciones.push(participacion)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            participacion:arregloParticipaciones,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            participacion:[]
        };
    }
}

const generarApoyos=() =>{
    let arregloApoyos=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const apoyo = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            tipoPersona:faker.random.arrayElement(tiposPersona),
            beneficiarioPrograma:faker.random.arrayElement(beneficiarios),
            nombrePrograma:`Programa ${faker.name.firstName()}`,
            institucionOtorgante:faker.random.arrayElement(instituciones),
            nivelOrdenGobierno:faker.random.arrayElement(ordenesGobierno),
            tipoApoyo:faker.random.arrayElement(tiposApoyo),
            formaRecepcion:faker.random.arrayElement(formasRecepcion),
            montoApoyoMensual:generarMonto(),
            especifiqueApoyo:faker.random.words(8)
        }

        arregloApoyos.push(apoyo)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            apoyo:arregloApoyos,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            apoyo:[]
        };
    }
}

const generarRepresentacion=() =>{
    let arregloRepresentacion=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const representacion = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            tipoRelacion:faker.random.arrayElement(tiposRelacion),
            tipoRepresentacion:faker.random.arrayElement(tiposRepresentacion),
            fechaInicioRepresentacion:darFormatoFecha(faker.date.past()),
            tipoPersona:faker.random.arrayElement(tiposPersona),
            nombreRazonSocial:faker.random.arrayElement(empresas),
            rfc:generarRFC(),
            recibeRemuneracion:faker.datatype.boolean(),
            montoMensual:generarMonto(),
            ubicacion:{
                pais:"MX",
                entidadFederativa:faker.random.arrayElement(entidadesFederativas),
            },
            sector:faker.random.arrayElement(sectores)
        }

        arregloRepresentacion.push(representacion)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            representacion:arregloRepresentacion,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            representacion:[]
        };
    }
}

const generarClientesPrincipales=() =>{
    let arregloClientes=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const cliente = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            realizaActividadLucrativa:faker.datatype.boolean(),
            tipoRelacion:faker.random.arrayElement(tiposRelacion),
            empresa:{
                nombreEmpresaServicio:faker.random.arrayElement(empresas),
                rfc:generarRFC(1)
            },
            clientePrincipal:{
                tipoPersona:faker.random.arrayElement(tiposPersona),
                nombreRazonSocial:faker.random.arrayElement(empresas),
                rfc:generarRFC(1)
            },
            sector:faker.random.arrayElement(sectores),
            montoAproximadoGanancia:generarMonto(),
            ubicacion:{
                pais:"MX",
                entidadFederativa:faker.random.arrayElement(entidadesFederativas),
            }            
        }

        arregloClientes.push(cliente)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            cliente:arregloClientes,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            cliente:[]
        };
    }
}

const generarBeneficiarios=() =>{

    let arregloBeneficiarios=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i <  limite; i++) {
        arregloBeneficiarios.push(faker.random.arrayElement(beneficiarios))
        arregloBeneficiarios=Array.from(new Set(arregloBeneficiarios));
            
    }
    return arregloBeneficiarios;
}

const generarBeneficiosPrivados=() =>{
    let arregloBeneficios=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const beneficio = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            tipoPersona:faker.random.arrayElement(tiposPersona),
            tipoBeneficio:faker.random.arrayElement(tiposBeneficio),
            beneficiario:generarBeneficiarios(),
            otorgante:{
                tipoPersona:faker.random.arrayElement(tiposPersona),
                nombreRazonSocial:faker.random.arrayElement(empresas),
                rfc:generarRFC(1)
            },
            formaRecepcion:faker.random.arrayElement(formasRecepcion),
            especifiqueBeneficio:faker.random.words(8),
            montoMensualAproximado:generarMonto(),
            sector:faker.random.arrayElement(sectores),    
        }

        arregloBeneficios.push(beneficio)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            beneficio:arregloBeneficios,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            beneficio:[]
        };
    }
}

const generarFideicomisos=() =>{
    let arregloFideicomisos=[];
    let limite=faker.datatype.number({ min: 1, max:  2});

    for (let i = 0; i < limite; i++) {
        const fideicomiso = {
            tipoOperacion:faker.random.arrayElement(tiposOperacion),
            tipoRelacion:faker.random.arrayElement(tiposRelacion), 
            tipoFideicomiso:faker.random.arrayElement(tiposFideicomisos), 
            tipoParticipacion:faker.random.arrayElement(tiposParticipacionFideicomisos),
            rfc:generarRFC(1),
            fideicomitente:{
                tipoPersona:faker.random.arrayElement(tiposPersona),
                nombreRazonSocial:faker.random.arrayElement(empresas),
                rfc:generarRFC(1)
            }, 
            fiduciario:{
                nombreRazonSocial:faker.random.arrayElement(empresas),
                rfc:generarRFC(1)
            },
            fideicomisario:{
                tipoPersona:faker.random.arrayElement(tiposPersona),
                nombreRazonSocial:faker.random.arrayElement(empresas),
                rfc:generarRFC(1)
            }, 
            sector:faker.random.arrayElement(sectores),
            extranjero:faker.random.arrayElement(extranjero)
        }

        arregloFideicomisos.push(fideicomiso)
    }

    let bandera=faker.datatype.boolean();

    if(bandera==false){
        return {
            ninguno:bandera,
            fideicomiso:arregloFideicomisos,
            aclaracionesObservaciones:faker.random.words(8),
        };
    }else{
          return {
            ninguno:bandera,
            fideicomiso:[]
        };
    }
}

const generarActualizacion=()=>{
    let fecha= faker.date.past().toISOString().substr(0,19);
    return fecha+"Z";
}

//FUNCIÓN PRINCIPAL-----------------------------------------------------
const generarDeclaraciones=() => {
    
    let declaraciones = [];
    let tipo=argv.t;
    let todos=false;

    switch (tipo) {
        case "i":
            tipo="INICIAL"
        break;
        case "m":
            tipo="MODIFICACIÓN"
        break;

        case "c":
            tipo="CONCLUSIÓN"
        break;

        case "t":
            todos=true;
        break;
    }

    console.log(`Generando ${argv.d} declaraciones, por favor espere...`.yellow)
    for (let i =0; i < argv.d; i++) {
        
        let declaracion;

        if(todos==true){
            tipo=faker.random.arrayElement(tiposDeclaracion);
        }

        switch (tipo) {
            case "INICIAL":
                declaracion={
                    id: faker.datatype.uuid(),
                    metadata:generarMetadata(tipo),
                    declaracion:{
                        situacionPatrimonial:{
                            datosGenerales:generarDatosGenerales(),
                            domicilioDeclarante:generarDomicilioDeclarante(),
                            datosCurricularesDeclarante:generarDatosCurriculares(),
                            datosEmpleoCargoComision:generarDatosEmpleoCargoComision(0),
                            experienciaLaboral:generarExperiencia(),
                            datosPareja:generarDatosPareja(),
                            datosDependienteEconomico:generarDatosDependienteEconomico(),
                            ingresos:{
                                remuneracionMensualCargoPublico:generarMonto(),
                                otrosIngresosMensualesTotal:generarMonto(),
                                actividadIndustialComercialEmpresarial:{
                                    remuneracionTotal:generarMonto(),
                                    actividades:generarActividadesIndustriales(),
                                },
                                actividadFinanciera:{
                                    remuneracionTotal:generarMonto(),
                                    actividades:generarActividadesFinancieras(),
                                },
                                serviciosProfesionales:{
                                    remuneracionTotal:generarMonto(),
                                    servicios:generarServiciosProfesionales(),
                                },
                                otrosIngresos:{
                                    remuneracionTotal:generarMonto(),
                                    ingresos:generarIngresos()
                                },
                                ingresoMensualNetoDeclarante:generarMonto(),
                                ingresoMensualNetoParejaDependiente:generarMonto(),
                                totalIngresosMensualesNetos:generarMonto(), 
                                aclaracionesObservaciones:faker.random.words(8), 
                            },
                            actividadAnualAnterior:generarActividadAnualInterior(),
                            bienesInmuebles:generarBienesInmuebles(),
                            vehiculos:generarVehiculos(),
                            bienesMuebles:generarBienesMuebles(),
                            inversiones:generarInversiones(0),
                            adeudos:generarAdeudos(0),
                            prestamoOComodato:generarPrestamosOComodatos(),
                        },
                        interes:{
                            participacion:generarParticipaciones(),
                            participacionTomaDecisiones:generarParticipacionesTomaDecisiones(),
                            apoyos:generarApoyos(),
                            representacion:generarRepresentacion(),
                            clientesPrincipales:generarClientesPrincipales(),
                            beneficiosPrivados:generarBeneficiosPrivados(),
                            fideicomisos:generarFideicomisos()
                        }
                    }
                }
            break;

            case "MODIFICACIÓN":
                declaracion={
                    id: faker.datatype.uuid(),
                    metadata:generarMetadata(tipo),
                    declaracion:{
                        situacionPatrimonial:{
                            datosGenerales:generarDatosGenerales(),
                            domicilioDeclarante:generarDomicilioDeclarante(),
                            datosCurricularesDeclarante:generarDatosCurriculares(),
                            datosEmpleoCargoComision:generarDatosEmpleoCargoComision(1),
                            experienciaLaboral:generarExperiencia(),
                            datosPareja:generarDatosPareja(),
                            datosDependienteEconomico:generarDatosDependienteEconomico(),
                            ingresos:{
                                remuneracionAnualCargoPublico:generarMonto(),
                                otrosIngresosAnualesTotal:generarMonto(),
                                actividadIndustialComercialEmpresarial:{
                                    remuneracionTotal:generarMonto(),
                                    actividades:generarActividadesIndustriales(),
                                },
                                actividadFinanciera:{
                                    remuneracionTotal:generarMonto(),
                                    actividades:generarActividadesFinancieras(),
                                },
                                serviciosProfesionales:{
                                    remuneracionTotal:generarMonto(),
                                    servicios:generarServiciosProfesionales(),
                                },
                                enajenacionBienes:{
                                    remuneracionTotal:generarMonto(),
                                    bienes:generarBienes(),
                                },
                                otrosIngresos:{
                                    remuneracionTotal:generarMonto(),
                                    ingresos:generarIngresos()
                                },
                                ingresoAnualNetoDeclarante:generarMonto(),
                                ingresoAnualNetoParejaDependiente:generarMonto(),
                                totalIngresosAnualesNetos:generarMonto(), 
                                aclaracionesObservaciones:faker.random.words(8), 
                            },
                            actividadAnualAnterior:generarActividadAnualInterior(),
                            bienesInmuebles:generarBienesInmuebles(),
                            vehiculos:generarVehiculos(),
                            bienesMuebles:generarBienesMuebles(),
                            inversiones:generarInversiones(1),
                            adeudos:generarAdeudos(1),
                            prestamoOComodato:generarPrestamosOComodatos(),
                        },
                        interes:{
                            participacion:generarParticipaciones(),
                            participacionTomaDecisiones:generarParticipacionesTomaDecisiones(),
                            apoyos:generarApoyos(),
                            representacion:generarRepresentacion(),
                            clientesPrincipales:generarClientesPrincipales(),
                            beneficiosPrivados:generarBeneficiosPrivados(),
                            fideicomisos:generarFideicomisos()
                        }
                    }
                }
            break;

            case "CONCLUSIÓN":
                declaracion={
                    id: faker.datatype.uuid(),
                    metadata:generarMetadata(tipo),
                    declaracion:{
                        situacionPatrimonial:{
                            datosGenerales:generarDatosGenerales(),
                            domicilioDeclarante:generarDomicilioDeclarante(),
                            datosCurricularesDeclarante:generarDatosCurriculares(),
                            datosEmpleoCargoComision:generarDatosEmpleoCargoComision(2),
                            experienciaLaboral:generarExperiencia(),
                            datosPareja:generarDatosPareja(),
                            datosDependienteEconomico:generarDatosDependienteEconomico(),
                            ingresos:{
                                remuneracionConclusionCargoPublico:generarMonto(),
                                otrosIngresosConclusionTotal:generarMonto(),
                                actividadIndustialComercialEmpresarial:{
                                    remuneracionTotal:generarMonto(),
                                    actividades:generarActividadesIndustriales(),
                                },
                                actividadFinanciera:{
                                    remuneracionTotal:generarMonto(),
                                    actividades:generarActividadesFinancieras(),
                                },
                                serviciosProfesionales:{
                                    remuneracionTotal:generarMonto(),
                                    servicios:generarServiciosProfesionales(),
                                },
                                otrosIngresos:{
                                    remuneracionTotal:generarMonto(),
                                    ingresos:generarIngresos()
                                },
                                ingresoConclusionNetoDeclarante:generarMonto(),
                                ingresoConclusionNetoParejaDependiente:generarMonto(),
                                totalIngresosConclusionNetos:generarMonto(), 
                                aclaracionesObservaciones:faker.random.words(8), 
                            },
                            actividadAnualAnterior:generarActividadAnualInterior(),
                            bienesInmuebles:generarBienesInmuebles(),
                            vehiculos:generarVehiculos(),
                            bienesMuebles:generarBienesMuebles(),
                            inversiones:generarInversiones(2),
                            adeudos:generarAdeudos(2),
                            prestamoOComodato:generarPrestamosOComodatos(),
                        },
                        interes:{
                            participacion:generarParticipaciones(),
                            participacionTomaDecisiones:generarParticipacionesTomaDecisiones(),
                            apoyos:generarApoyos(),
                            representacion:generarRepresentacion(),
                            clientesPrincipales:generarClientesPrincipales(),
                            beneficiosPrivados:generarBeneficiosPrivados(),
                            fideicomisos:generarFideicomisos()
                        }
                    }
                }
            break;
        }

        declaraciones.push(declaracion);
    }

    declaraciones.length==1 ? console.log(`Se generó ${declaraciones.length} declaración`.green) : console.log(`Se generaron ${declaraciones.length} declaraciones`.green); 
    
    return declaraciones;
}


if (!fs.existsSync("datos")){
    fs.mkdirSync("datos");
}
fs.writeFileSync('datos/datosDeclaraciones.json', JSON.stringify(generarDeclaraciones(), null, "\t"))
